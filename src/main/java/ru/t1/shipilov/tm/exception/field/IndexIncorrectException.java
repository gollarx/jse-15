package ru.t1.shipilov.tm.exception.field;

public class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}
